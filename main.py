from turtle import Screen
import time
from vibora import Vibora
from food import Comida
from PantallaPuntuacion import PantallaPuntuacion
puntuacion=PantallaPuntuacion()
pantalla = Screen()
pantalla.setup(width=600, height=600)
pantalla.bgcolor("black")
pantalla.title("Juego de la viborita")
pantalla.tracer(n=0)
comida = Comida()
vibora = Vibora()
hay_juego = True
puntuacion.mostrar_puntos()

while hay_juego:
    pantalla.update()
    time.sleep(0.2)
    vibora.mover()
    pantalla.listen()
    pantalla.onkeypress(vibora.mover_arriba, "w")
    pantalla.onkeypress(vibora.mover_abajo, "s")
    pantalla.onkeypress(vibora.mover_izquierda, "a")
    pantalla.onkeypress(vibora.mover_derecha, "d")
    if vibora.comer(comida):
        comida.reemplazar_comida()
        puntuacion.incrementar_puntos()
        puntuacion.mostrar_puntos()
    #hay_juego = vibora.calcular_perder()
    if vibora.calcular_perder():
        puntuacion.reset()
        vibora.reset()

pantalla.exitonclick()
