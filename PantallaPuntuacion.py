from turtle import Turtle


class PantallaPuntuacion(Turtle):
    puntuacion = 0
    mayor_puntuacion = 0

    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.color("white")
        self.penup()
        self.setpos(x=-115, y=270)
        self.leer_puntuacion()

    def mostrar_puntos(self):
        self.clear()
        self.write(arg=f"Puntuacion: {self.puntuacion} Mayor puntuacion: {self.mayor_puntuacion}", align="left",
                   font=("arial", 12, "normal"))

    def incrementar_puntos(self):
        self.clear()
        self.puntuacion += 1

    def reset(self):
        if self.puntuacion > self.mayor_puntuacion:
            self.mayor_puntuacion = self.puntuacion
            self.guardar_puntuacion()
        self.puntuacion = 0
        self.mostrar_puntos()

    def leer_puntuacion(self):
        with open("mayor_puntuacion.txt") as archivo:
            self.mayor_puntuacion = int(archivo.read())

    def guardar_puntuacion(self):
        with open("mayor_puntuacion.txt", "w") as archivo:
            archivo.write(str(self.mayor_puntuacion))
