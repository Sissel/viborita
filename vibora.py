from turtle import Turtle


class Vibora:
    arriba = 90
    izquierda = 180
    abajo = 270
    derecha = 0
    puntos = 0

    def __init__(self):
        self.vibora = []
        self.crear_vibora()

    def crear_vibora(self):
        x = 0
        y = 0
        for pedazo_de_vibora in range(3):
            pedazo_de_vibora = Turtle()
            pedazo_de_vibora.penup()
            pedazo_de_vibora.setpos(x, y)
            pedazo_de_vibora.shape("square")
            pedazo_de_vibora.color("white")
            x -= 20
            self.vibora.append(pedazo_de_vibora)

    def mover(self):
        for pedazo_de_vibora in range(len(self.vibora) - 1, -1, -1):
            if pedazo_de_vibora == 0:
                self.vibora[pedazo_de_vibora].forward(20)
            else:
                x = self.vibora[pedazo_de_vibora - 1].xcor()
                y = self.vibora[pedazo_de_vibora - 1].ycor()
                self.vibora[pedazo_de_vibora].goto(x=x, y=y)

    def comer(self, comida):
        x = comida.xcor()
        y = comida.ycor()
        if self.vibora[0].distance(x, y) < 5:
            # print (f"coordenada x de la comida: {x}")
            # print (f"coodenada y de la comida: {y}")
            # print (f"coodenada x del a vibora: {self.vibora[0].xcor()}")
            # print (f"coodenada y del a vibora: {self.vibora[0].ycor()}")
            self.crecer()
            return True

    def crecer(self):
        ultima = len(self.vibora) - 1
        x = self.vibora[ultima].xcor()
        y = self.vibora[ultima].ycor()
        pedazo_de_vibora = Turtle()
        pedazo_de_vibora.penup()
        pedazo_de_vibora.setpos(x, y)
        pedazo_de_vibora.shape("square")
        pedazo_de_vibora.color("white")
        self.vibora.append(pedazo_de_vibora)

    def calcular_perder(self):
        if self.vibora[0].xcor() > 290 or self.vibora[0].xcor() < -290:
            return True
        elif self.vibora[0].ycor() > 290 or self.vibora[0].ycor() < -290:
            return True
        elif self.detectar_colision_cola():
            return True
        else:
            return False

    def detectar_colision_cola(self):
        for pedazo_de_vibora in self.vibora[1:]:
            if pedazo_de_vibora.distance(self.vibora[0]) < 10:
                return True
        # for pedazo_de_vibora in range(len(self.vibora) - 1, 0, -1):
        #     if self.vibora[pedazo_de_vibora].distance(self.vibora[0]) < 10:
        #         return True

    def mover_arriba(self):
        if self.vibora[0].heading() != self.abajo:
            self.vibora[0].setheading(90)

    def mover_derecha(self):
        if self.vibora[0].heading() != self.izquierda:
            self.vibora[0].seth(0)

    def mover_abajo(self):
        if self.vibora[0].heading() != self.arriba:
            self.vibora[0].seth(270)

    def mover_izquierda(self):
        if self.vibora[0].heading() != self.derecha:
            self.vibora[0].seth(180)

    def reset(self):
        for pedazo_vibora in self.vibora:
            pedazo_vibora.hideturtle()
        self.vibora = []
        self.crear_vibora()
