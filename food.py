from turtle import Turtle
import random


class Comida(Turtle):
    coordenadaX = []
    coordenadaY = []

    def __init__(self):
        super().__init__()
        self.crear_comida()

    def crear_comida(self):
        self.calcular_coordenadas()
        self.penup()
        self.color("blue")
        self.setx(random.choice(self.coordenadaX))
        self.sety(random.choice(self.coordenadaY))
        self.shape("circle")

    def reemplazar_comida(self):
        self.calcular_coordenadas()
        self.setx(random.choice(self.coordenadaX))
        self.sety(random.choice(self.coordenadaY))


    def calcular_coordenadas(self):
        for coordenada in range(-280, 300, 20):
            self.coordenadaX.append(coordenada)
            self.coordenadaY.append(coordenada)
